To start application locally following steps must be done. 
**Prerequisites:**

    1. Install jdk 18
    2. Install maven 3.8.5
    3. Install postgres 14
    4. Create database "project"

**Start spring boot application with following env variables:** 

    spring.datasource.username=<username>
    spring.datasource.password=<password>

By default, application is running on port 8080 and database port 5432 (override in application.yml)

**For starting tests locally:**
   1. Create database "project_test".
   2. Go to application-test.yml file, setup database username and password.

    spring.datasource.username=<username>
    spring.datasource.password=<password>

By default, mock server is running on port 8181. You can override it by changing property hnb.port application-test.yml file. 
Database port for tests is 5432.




    