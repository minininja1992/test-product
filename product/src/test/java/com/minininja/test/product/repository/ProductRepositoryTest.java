package com.minininja.test.product.repository;

import com.minininja.test.product.RepositoryTestBase;
import com.minininja.test.product.utils.ProductTestConstants;
import org.junit.jupiter.api.Test;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;


public class ProductRepositoryTest extends RepositoryTestBase {

    @Test
    public void should_get_product_by_id() {
        var givenProduct = productRepository.save(ProductTestConstants.givenValidProduct(null));

        var product = productRepository.findById(givenProduct.getId()).get();

        assertThat(product.getId()).isEqualTo(givenProduct.getId());
    }

    @Test
    public void should_delete_product_by_id() {
        var givenProduct = productRepository.save(ProductTestConstants.givenValidProduct(null));

        productRepository.deleteById(givenProduct.getId());

        assertThat(productRepository.findById(givenProduct.getId())).isEqualTo(Optional.empty());
    }

    @Test
    public void should_create_new_product() {
        var givenProduct = ProductTestConstants.givenValidProduct(null);

        var savedProduct = productRepository.save(givenProduct);

        assertThat(savedProduct).usingRecursiveComparison().isEqualTo(givenProduct);
    }

    @Test
    public void should_update_product() {
        final var givenDescription = "novi description";
        var givenProduct = productRepository.save(ProductTestConstants.givenValidProduct(null));
        givenProduct.setDescription(givenDescription);

        var savedProduct = productRepository.save(givenProduct);

        assertThat(savedProduct.getDescription()).isEqualTo(givenDescription);
        assertThat(savedProduct.getId()).isEqualTo(givenProduct.getId());
    }

}
