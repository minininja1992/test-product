package com.minininja.test.product;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.tomakehurst.wiremock.WireMockServer;
import com.minininja.test.product.model.dto.ProductRequest;
import com.minininja.test.product.utils.ProductTestConstants;
import org.eclipse.jetty.http.HttpStatus;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.jdbc.JdbcTestUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.math.BigDecimal;
import java.util.List;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.options;
import static com.minininja.test.product.utils.ProductTestConstants.SAVE_PATH;
import static org.springframework.http.HttpHeaders.CONTENT_TYPE;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ActiveProfiles("test")
@SpringBootTest(classes = ProductApplication.class)
@AutoConfigureMockMvc
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class ProductIntegrationTest {

    private static ObjectMapper jsonMapper = new ObjectMapper();
    private static WireMockServer mockServer = new WireMockServer(options().port(8181).bindAddress("localhost"));
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    JdbcTemplate jdbcTemplate;

    private static final String httpLocalhost = "http://localhost:8080/" + SAVE_PATH;


    @BeforeEach
    public void webServer() {
        this.mockServer.start();
        configureFor("localhost", this.mockServer.port());
    }

    @AfterEach
    public void deleteTests() {
        this.mockServer.stop();
        JdbcTestUtils.deleteFromTables(jdbcTemplate, "Product");
    }

    @Test
    public void should_save_new_product() throws Exception {
        stubFor(get("/tecajn/v2?valuta=EUR").withPort(8181)
                .willReturn(aResponse().withHeader(CONTENT_TYPE, APPLICATION_JSON_VALUE)
                        .withStatus(HttpStatus.OK_200).withBody(objectToJson(List.of(ProductTestConstants.givenEurExchangeRateResponse())))));

        mockMvc.perform(MockMvcRequestBuilders.post(httpLocalhost)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectToJson(ProductTestConstants.givenValidProductRequest(null)))
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON));
    }

    @Test
    public void should_throw_null_violation_exceptions_on_save_product() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post(httpLocalhost)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectToJson(new ProductRequest(null, null, null, null, null, false)))
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(content().json("{\"status\":\"BAD_REQUEST\",\"message\":[\"priceHrk: priceHrk is required\",\"code: code is required\"]}"))
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON));
    }

    @Test
    public void should_return_bad_request_for_non_valid_price_and_code_on_save_product() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post(httpLocalhost)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectToJson(new ProductRequest(null, "1233", null, BigDecimal.valueOf(-1), null, false)))
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(content().json("{\"status\":\"BAD_REQUEST\",\"message\":[\"code: code must be exactly 10 chars\",\"priceHrk: value must not be negative\"]}"))
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON));
    }

    @Test
    public void should_return_bad_request_for_negative_id_and_price_on_save_product() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post(httpLocalhost)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectToJson(new ProductRequest(-1, "1234567890", null, BigDecimal.valueOf(-1), null, false)))
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andExpect(content().json("{\"status\":\"BAD_REQUEST\",\"message\":[\"id: id must be positive\",\"priceHrk: value must not be negative\"]}"))
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON));
    }

    @Test
    public void should_return_dependency_failed_for_5xx_error_on_save_product() throws Exception {
        stubFor(get("/tecajn/v2?valuta=EUR").withPort(8181)
                .willReturn(aResponse().withHeader(CONTENT_TYPE, APPLICATION_JSON_VALUE)
                        .withStatus(HttpStatus.INTERNAL_SERVER_ERROR_500).withBody("\"message\": \"message\"")));

        mockMvc.perform(MockMvcRequestBuilders.post(httpLocalhost)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectToJson(ProductTestConstants.givenValidProduct(null)))
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isFailedDependency())
                .andExpect(content().json("{\"status\":\"FAILED_DEPENDENCY\",\"message\":[\"Invalid response from hnb api\"]}"))
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON));
    }

    @Test
    public void should_return_dependency_failed_for_4xx_error_on_save_product() throws Exception {
        stubFor(get("/tecajn/v2?valuta=EUR").withPort(8181)
                .willReturn(aResponse().withHeader(CONTENT_TYPE, APPLICATION_JSON_VALUE)
                        .withStatus(HttpStatus.FORBIDDEN_403).withBody("\"message\": \"message\"")));

        mockMvc.perform(MockMvcRequestBuilders.post(httpLocalhost)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectToJson(ProductTestConstants.givenValidProduct(null)))
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isFailedDependency())
                .andExpect(content().json("{\"status\":\"FAILED_DEPENDENCY\",\"message\":[\"Invalid response from hnb api\"]}"))
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON));
    }

    @Test
    public void should_return_dependency_failed_for_timeout_on_product_save() throws Exception {
        stubFor(get("/tecajn/v2?valuta=EUR").withPort(8181)
                .willReturn(aResponse().withHeader(CONTENT_TYPE, APPLICATION_JSON_VALUE)
                        .withFixedDelay(5000).withStatus(HttpStatus.OK_200).withBody("\"\\\"message\\\": \\\"message\\\"\"")));

        mockMvc.perform(MockMvcRequestBuilders.post(httpLocalhost)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectToJson(ProductTestConstants.givenValidProduct(null)))
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isFailedDependency())
                .andExpect(content().json("{\"status\":\"FAILED_DEPENDENCY\",\"message\":[\"Invalid response from hnb api\"]}"))
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON));
    }

    private static String objectToJson(Object object) {
        try {
            return jsonMapper.writeValueAsString(object);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }
}
