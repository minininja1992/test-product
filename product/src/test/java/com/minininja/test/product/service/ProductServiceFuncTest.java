package com.minininja.test.product.service;

import com.minininja.test.product.model.database.Product;
import com.minininja.test.product.model.external.HnbExchangeRateResponse;
import com.minininja.test.product.repository.ProductRepository;
import com.minininja.test.product.utils.ProductTestConstants;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.web.server.ResponseStatusException;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static com.minininja.test.product.utils.ProductTestConstants.EUR_CURRENCY;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class ProductServiceFuncTest {

    @Mock
    ProductRepository productRepository;

    @Mock
    HnbService hnbService;

    @InjectMocks
    ProductService productService;

    @Test
    public void should_throw_if_not_found_by_id() {
        when(productRepository.findById(anyInt())).thenReturn(Optional.empty());

        assertThatExceptionOfType(ResponseStatusException.class)
                .isThrownBy(() -> productService.getById(1))
                .withMessageContaining("Unable to find product with id");
    }

    @Test
    public void should_throw_if_deleting_product_does_not_exist() {
        when(productRepository.findById(anyInt())).thenReturn(Optional.empty());

        assertThatExceptionOfType(ResponseStatusException.class)
                .isThrownBy(() -> productService.deleteById(1))
                .withMessageContaining("Unable to find product with id");
    }

    @Test
    public void should_throw_update_request_if_not_exist() {
        when(productRepository.findById(anyInt())).thenReturn(Optional.empty());

        assertThatExceptionOfType(ResponseStatusException.class)
                .isThrownBy(() -> productService.save(ProductTestConstants.givenValidProductRequest(1)))
                .withMessageContaining("Unable to find product with id");
    }

    @Test
    public void should_throw_if_hnb_reponse_is_empty() {
        when(productRepository.findById(any())).thenReturn(Optional.of(ProductTestConstants.givenValidProduct(1)));
        when(hnbService.getExchangeRates(Set.of(EUR_CURRENCY))).thenReturn(List.of(new HnbExchangeRateResponse("EUR", BigDecimal.TEN, null, BigDecimal.TEN)));

        assertThatExceptionOfType(ResponseStatusException.class)
                .isThrownBy(() -> productService.save(ProductTestConstants.givenValidProductRequest(1)))
                .withMessageContaining("Invalid response from hnb api");
    }

    @Test
    public void should_throw_if_hnb_response_does_not_contain_eur() {
        when(productRepository.findById(any())).thenReturn(Optional.of(ProductTestConstants.givenValidProduct(1)));
        when(hnbService.getExchangeRates(Set.of(EUR_CURRENCY))).thenReturn(List.of(new HnbExchangeRateResponse("USD", BigDecimal.TEN, BigDecimal.TEN, BigDecimal.TEN)));

        assertThatExceptionOfType(ResponseStatusException.class)
                .isThrownBy(() -> productService.save(ProductTestConstants.givenValidProductRequest(1)))
                .withMessageContaining("Invalid response from hnb api");
    }

    @Test
    public void should_throw_if_exchance_eur_price_is_null() {
        when(productRepository.findById(any())).thenReturn(Optional.of(ProductTestConstants.givenValidProduct(1)));
        when(hnbService.getExchangeRates(Set.of(EUR_CURRENCY))).thenReturn(List.of());

        assertThatExceptionOfType(ResponseStatusException.class)
                .isThrownBy(() -> productService.save(ProductTestConstants.givenValidProductRequest(1)))
                .withMessageContaining("Invalid response from hnb api");
    }

    @Test
    public void should_save_new_product() {
        when(productRepository.save(any())).thenReturn(ProductTestConstants.givenValidProduct(1));
        when(hnbService.getExchangeRates(Set.of(EUR_CURRENCY))).thenReturn(List.of(ProductTestConstants.givenEurExchangeRateResponse()));
        var productArgumentCaptor = ArgumentCaptor.forClass(Product.class);

        productService.save(ProductTestConstants.givenValidProductRequest(null));

        verify(hnbService, times(1)).getExchangeRates(eq(Set.of(EUR_CURRENCY)));
        verify(productRepository, times(0)).findById(anyInt());
        verify(productRepository, times(1)).save(productArgumentCaptor.capture());
        assertThat(productArgumentCaptor.getValue().getPriceEur()).isEqualTo(BigDecimal.valueOf(100));
    }

    @Test
    public void should_update_product() {
        when(productRepository.findById(any())).thenReturn(Optional.of(ProductTestConstants.givenValidProduct(1)));
        when(productRepository.save(any())).thenReturn(ProductTestConstants.givenValidProduct(1));
        when(hnbService.getExchangeRates(Set.of(EUR_CURRENCY))).thenReturn((List.of(ProductTestConstants.givenEurExchangeRateResponse())));
        var productArgumentCaptor = ArgumentCaptor.forClass(Product.class);

        productService.save(ProductTestConstants.givenValidProductRequest(1));

        verify(hnbService, times(1)).getExchangeRates(eq(Set.of(EUR_CURRENCY)));
        verify(productRepository, times(1)).findById(anyInt());
        verify(productRepository, times(1)).save(productArgumentCaptor.capture());
        assertThat(productArgumentCaptor.getValue().getId()).isEqualTo(1);
        assertThat(productArgumentCaptor.getValue().getPriceEur()).isEqualTo(BigDecimal.valueOf(100));
    }

    @Test
    public void should_not_call_hnb_service_if_hrk_price_is_zero() {
        when(productRepository.save(any())).thenReturn(ProductTestConstants.givenValidProduct(1));
        var productArgumentCaptor = ArgumentCaptor.forClass(Product.class);

        productService.save(ProductTestConstants.givenValidProductRequest(null, BigDecimal.ZERO));

        verify(hnbService, times(0)).getExchangeRates(any());
        verify(productRepository, times(1)).save(productArgumentCaptor.capture());
        assertThat(productArgumentCaptor.getValue().getPriceEur()).isEqualTo(BigDecimal.ZERO);
        assertThat(productArgumentCaptor.getValue().getPriceHrk()).isEqualTo(BigDecimal.ZERO);
    }

    @Test
    public void should_throw_on_duplicated_code_constraint_exception() {
        when(productRepository.save(any())).thenThrow(DataIntegrityViolationException.class);
        when(hnbService.getExchangeRates(Set.of(EUR_CURRENCY))).thenReturn(List.of(ProductTestConstants.givenEurExchangeRateResponse()));

        assertThatExceptionOfType(ResponseStatusException.class).isThrownBy(() -> productService.save(ProductTestConstants.givenValidProductRequest(null)))
                .withMessageContaining("Product with code already exists");

        verify(productRepository, times(1)).save(any());
    }
}
