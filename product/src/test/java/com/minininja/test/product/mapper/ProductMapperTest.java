package com.minininja.test.product.mapper;

import com.minininja.test.product.model.database.Product;
import com.minininja.test.product.utils.ProductTestConstants;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static org.assertj.core.api.Assertions.assertThat;

public class ProductMapperTest {


    @Test
    public void should_map_from_product_request() {
        var givenProductRequest = ProductTestConstants.givenValidProductRequest(1);
        var givenEurPrice = BigDecimal.TEN;

        Product product = ProductMapper.mapFromProductRequest(givenProductRequest, givenEurPrice);

        assertThat(givenProductRequest.id()).isEqualTo(product.getId());
        assertThat(givenProductRequest.code()).isEqualTo(product.getCode());
        assertThat(givenProductRequest.name()).isEqualTo(product.getName());
        assertThat(givenProductRequest.priceHrk()).isEqualTo(product.getPriceHrk());
        assertThat(givenEurPrice).isEqualTo(product.getPriceEur());
        assertThat(givenProductRequest.description()).isEqualTo(product.getDescription());
        assertThat(givenProductRequest.available()).isEqualTo(product.isAvailable());
    }

    @Test
    public void should_map_to_product_response() {
        var givenProduct = ProductTestConstants.givenValidProduct(1);

        var productResponse = ProductMapper.mapToProductResponse(givenProduct);

        assertThat(productResponse.id()).isEqualTo(givenProduct.getId());
        assertThat(productResponse.code()).isEqualTo(givenProduct.getCode());
        assertThat(productResponse.name()).isEqualTo(givenProduct.getName());
        assertThat(productResponse.priceHrk()).isEqualTo(givenProduct.getPriceHrk());
        assertThat(productResponse.priceEur()).isEqualTo(givenProduct.getPriceEur());
        assertThat(productResponse.description()).isEqualTo(givenProduct.getDescription());
        assertThat(productResponse.available()).isEqualTo(givenProduct.isAvailable());
    }

}
