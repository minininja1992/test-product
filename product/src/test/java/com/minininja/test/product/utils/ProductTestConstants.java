package com.minininja.test.product.utils;

import com.minininja.test.product.model.Currency;
import com.minininja.test.product.model.database.Product;
import com.minininja.test.product.model.dto.ProductRequest;
import com.minininja.test.product.model.external.HnbExchangeRateResponse;

import java.math.BigDecimal;

import static com.minininja.test.product.controller.Paths.*;

public class ProductTestConstants {

    public static final BigDecimal HRK_PRICE = BigDecimal.TEN;
    public static final BigDecimal EUR_PRICE = BigDecimal.valueOf(100);

    public static final String SAVE_PATH = PRODUCT_V1_API_PATH + SAVE_PRODUCT_PATH;
    public static final String DELETE_PATH = PRODUCT_V1_API_PATH + DELETE_BY_ID_PATH;
    public static final String GET_PATH = PRODUCT_V1_API_PATH + GET_BY_ID_PATH;

    public static final Currency EUR_CURRENCY = Currency.EUR;

    public static Product givenValidProduct(Integer id) {
        var product = new Product();

        product.setId(id);
        product.setCode("desetdeset");
        product.setName("deset");
        product.setPriceHrk(HRK_PRICE);
        product.setPriceEur(EUR_PRICE);
        product.setDescription("description");
        product.setAvailable(Boolean.FALSE);

        return product;
    }

    public static ProductRequest givenValidProductRequest(Integer id) {
        return givenValidProductRequest(id, HRK_PRICE);
    }

    public static ProductRequest givenValidProductRequest(Integer id, BigDecimal hrkPrice) {
        return new ProductRequest(id, "desetdeset", "deset", hrkPrice, "description", Boolean.FALSE);
    }

    public static HnbExchangeRateResponse givenEurExchangeRateResponse() {
        return new HnbExchangeRateResponse("EUR", BigDecimal.TEN, BigDecimal.TEN, BigDecimal.TEN);
    }

}
