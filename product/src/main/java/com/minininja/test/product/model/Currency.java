package com.minininja.test.product.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum Currency {
    EUR("EUR");

    public final String currencyCode;
}
