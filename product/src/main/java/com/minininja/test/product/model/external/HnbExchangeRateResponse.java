package com.minininja.test.product.model.external;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.minininja.test.product.utils.StringDecimalWithCommaToBigDecimalDeserializer;

import java.math.BigDecimal;


public record HnbExchangeRateResponse(@JsonProperty("valuta")
                                      String currencyCode,
                                      @JsonDeserialize(using = StringDecimalWithCommaToBigDecimalDeserializer.class)
                                      @JsonProperty("kupovni_tecaj")
                                      BigDecimal buyingRate,
                                      @JsonDeserialize(using = StringDecimalWithCommaToBigDecimalDeserializer.class)
                                      @JsonProperty("srednji_tecaj")
                                      BigDecimal middleRate,
                                      @JsonDeserialize(using = StringDecimalWithCommaToBigDecimalDeserializer.class)
                                      @JsonProperty("prodajni_tecaj")
                                      BigDecimal sellingRate) {
}
