package com.minininja.test.product.webclient;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.client.reactive.ReactorClientHttpConnector;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.util.UriComponentsBuilder;
import reactor.netty.http.client.HttpClient;

import java.time.Duration;

@Configuration
public class WebClientConfig {

    @Bean
    public WebClient hnbWebClient(@Value("${hnb.host}") String hnbHost, @Value("${hnb.port:#{null}}") Integer hnbPort, @Value("${hnb.protocol}") String protocol) {
        var httpClient = HttpClient.create()
                .responseTimeout(Duration.ofMillis(3000))
                .disableRetry(Boolean.TRUE);

        var baseUrl = UriComponentsBuilder.newInstance()
                .scheme(protocol)
                .host(hnbHost)
                .port(hnbPort == null ? null : String.valueOf(hnbPort))
                .toUriString();

        return WebClient.builder()
                .baseUrl(baseUrl)
                .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
                .clientConnector(new ReactorClientHttpConnector(httpClient))
                .build();
    }
}
