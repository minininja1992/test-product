package com.minininja.test.product.service;

import com.minininja.test.product.mapper.ProductMapper;
import com.minininja.test.product.model.Currency;
import com.minininja.test.product.model.database.Product;
import com.minininja.test.product.model.dto.ProductRequest;
import com.minininja.test.product.model.dto.ProductResponse;
import com.minininja.test.product.repository.ProductRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.math.BigDecimal;
import java.util.Set;

@Service
@RequiredArgsConstructor
@Slf4j
public class ProductService {

    private final ProductRepository productRepository;
    private final HnbService hnbService;

    private static final Currency EUR_CURRENCY = Currency.EUR;

    public ProductResponse getById(Integer id) {
        var product = tryFindByIdOrThrow(id);

        return ProductMapper.mapToProductResponse(product);
    }

    public ProductResponse save(ProductRequest validatedRequest) {
        throwUpdateRequestIfProductNotExist(validatedRequest.id());

        var eurPrice = validatedRequest.priceHrk().compareTo(BigDecimal.ZERO) > 0 ? calculateEurPrice(validatedRequest.priceHrk()) : BigDecimal.ZERO;
        var product = ProductMapper.mapFromProductRequest(validatedRequest, eurPrice);


        try {
            var savedProduct = productRepository.save(product);

            return ProductMapper.mapToProductResponse(savedProduct);
        } catch (DataIntegrityViolationException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Product with code already exists, code: %s".formatted(validatedRequest.code()));
        }
    }

    public void deleteById(Integer id) {
        tryFindByIdOrThrow(id);
        productRepository.deleteById(id);
    }

    private void throwUpdateRequestIfProductNotExist(Integer id) {
        if (id == null) {
            return;
        }
        tryFindByIdOrThrow(id);
    }

    private Product tryFindByIdOrThrow(Integer id) {
        return productRepository.findById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Unable to find product with id {%d}".formatted(id)));
    }

    private BigDecimal calculateEurPrice(BigDecimal priceHrk) {
        var eurExchangeRateResponse = hnbService.getExchangeRates(Set.of(Currency.EUR)).stream()
                .filter(exchangeRateResponse -> exchangeRateResponse.currencyCode().equals(EUR_CURRENCY.getCurrencyCode()))
                .toList();

        if (eurExchangeRateResponse.size() != 1 || eurExchangeRateResponse.get(0).middleRate() == null) {
            log.error("Invalid response: {}", eurExchangeRateResponse);
            throw new ResponseStatusException(HttpStatus.FAILED_DEPENDENCY, "Invalid response from hnb api");
        }

        return priceHrk.multiply(eurExchangeRateResponse.get(0).middleRate());
    }
}
