package com.minininja.test.product.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.List;

public record ApiError(HttpStatus status,
                       List<String> message) {

    public ResponseEntity<Object> toResponseEntity() {
        return new ResponseEntity<Object>(this, status);
    }
}
