package com.minininja.test.product.controller;

public final class Paths {

    public static final String PRODUCT_V1_API_PATH = "api/v1/product";
    public static final String SAVE_PRODUCT_PATH = "/save";
    public static final String GET_BY_ID_PATH = "/getById/{id}";
    public static final String DELETE_BY_ID_PATH = "/deleteById/{id}";

}
