package com.minininja.test.product.model.dto;

import javax.validation.constraints.*;
import java.math.BigDecimal;

public record ProductRequest(@Positive(message = "id must be positive")
                             Integer id,
                             @NotNull(message = "code is required")
                             @Size(min = 10, max = 10, message = "code must be exactly 10 chars")
                             String code,
                             String name,
                             @NotNull(message = "priceHrk is required")
                             @DecimalMin(value = "0.0", message = "value must not be negative")
                             @Digits(integer = 18, fraction = 4)
                             BigDecimal priceHrk,
                             String description,
                             Boolean available) {
}
