package com.minininja.test.product.model.dto;

import java.math.BigDecimal;

public record ProductResponse(Integer id,
                              String code,
                              String name,
                              BigDecimal priceHrk,
                              BigDecimal priceEur,
                              String description,
                              boolean available) {
}
