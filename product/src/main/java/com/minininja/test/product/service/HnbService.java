package com.minininja.test.product.service;

import com.minininja.test.product.model.Currency;
import com.minininja.test.product.model.external.HnbExchangeRateResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.server.ResponseStatusException;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.Set;

@Component
@Slf4j
@RequiredArgsConstructor
public class HnbService {

    private final WebClient hnbWebClient;
    private static final String CURRENCY_PARAM_NAME = "valuta";
    private static final String CURRENCY_PATH = "/tecajn/v2";

    public List<HnbExchangeRateResponse> getExchangeRates(Set<Currency> currencies) {
        return hnbWebClient.method(HttpMethod.GET)
                .uri(uriBuilder -> uriBuilder.path(CURRENCY_PATH)
                        .queryParam(CURRENCY_PARAM_NAME, currencies.stream().map(Currency::getCurrencyCode).toArray())
                        .build())
                .retrieve()
                .onStatus(HttpStatus::is4xxClientError,
                        error -> Mono.error(new IllegalArgumentException("Invalid request sent")))
                .onStatus(HttpStatus::is5xxServerError,
                        error -> Mono.error(new ResponseStatusException(HttpStatus.FAILED_DEPENDENCY, "Dependency failed")))
                .bodyToMono(new ParameterizedTypeReference<List<HnbExchangeRateResponse>>() {
                })
                .doOnError((throwable -> log.error("error occurred", throwable)))
                .onErrorReturn(List.of())
                .block();
    }
}
