package com.minininja.test.product.mapper;

import com.minininja.test.product.model.database.Product;
import com.minininja.test.product.model.dto.ProductRequest;
import com.minininja.test.product.model.dto.ProductResponse;

import java.math.BigDecimal;


public class ProductMapper {

    public static Product mapFromProductRequest(ProductRequest productRequest, BigDecimal priceInEur) {
        var product = new Product();
        product.setId(productRequest.id());
        product.setCode(productRequest.code());
        product.setName(productRequest.name());
        product.setPriceHrk(productRequest.priceHrk());
        product.setPriceEur(priceInEur);
        product.setDescription(productRequest.description());
        product.setAvailable(productRequest.available());

        return product;
    }

    public static ProductResponse mapToProductResponse(Product product) {
        return new ProductResponse(product.getId(), product.getCode(), product.getName(),
                product.getPriceHrk(), product.getPriceEur(), product.getDescription(),
                product.isAvailable());
    }
}
