package com.minininja.test.product.controller;

import com.minininja.test.product.model.dto.ProductRequest;
import com.minininja.test.product.model.dto.ProductResponse;
import com.minininja.test.product.service.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import static com.minininja.test.product.controller.Paths.*;

@RestController
@RequiredArgsConstructor
@RequestMapping(PRODUCT_V1_API_PATH)
public class ProductController {

    private final ProductService productService;

    @GetMapping(GET_BY_ID_PATH)
    public ProductResponse getProduct(@NotNull @Valid @Min(1) @PathVariable Integer id) {
        return productService.getById(id);
    }

    @PostMapping(SAVE_PRODUCT_PATH)
    public ProductResponse saveProduct(@NotNull @Valid @RequestBody ProductRequest request) {
        return productService.save(request);
    }

    @DeleteMapping(DELETE_BY_ID_PATH)
    public ResponseEntity deleteById(@NotNull @Min(1) @PathVariable Integer id) {
        productService.deleteById(id);
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }
}

