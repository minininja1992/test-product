CREATE TABLE Product(
    id serial PRIMARY KEY NOT NULL,
	code VARCHAR(10) NOT NULL UNIQUE CHECK (LENGTH(code)=10),
	name VARCHAR(50),
	price_hrk DECIMAL(19,4) NOT NULL CHECK (price_hrk>=0),
	price_eur DECIMAL(19,4) NOT NULL CHECK (price_eur>=0),
	description VARCHAR(200),
	is_available boolean DEFAULT false
	);